import { Controller, Get, Param, NotFoundException } from '@nestjs/common';
import { OrderMapper } from 'src/Order/Service/OrderMapper';

@Controller()
export class ReportController {
  constructor(private readonly orderMapper: OrderMapper) {}

  @Get('/report/products/:date')
  async bestSellers(@Param('date') date: string) {
    const bestSellers = await this.orderMapper.getBestsellers(date);
    if (bestSellers) {
      return bestSellers;
    }
    throw new NotFoundException('Not found any bestsellers.');
  }

  @Get('/report/customer/:date')
  async bestBuyers(@Param('date') date: string) {
    const bestBuyers = await this.orderMapper.getBestBuyers(date);
    if (bestBuyers) {
      return bestBuyers;
    }
    throw new NotFoundException('Not found any bestbuyers.');
  }
}
