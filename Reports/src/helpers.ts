export function getElementWithHighestOccurence(array) {
  if (array.lenght < 1) {
    return null;
  }
  let max = 0;
  const mode = {
    id: null,
    count: 0,
  };
  array.reduce((acc, curr) => {
    if (curr in acc) {
      acc[curr]++;
    } else {
      acc[curr] = 1;
    }

    if (max < acc[curr]) {
      max = acc[curr];
      mode.id = curr;
      mode.count = acc[curr];
    }

    return acc;
  }, {});

  return mode;
}
