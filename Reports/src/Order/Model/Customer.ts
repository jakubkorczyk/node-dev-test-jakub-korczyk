export interface Customer {
  firstName: string;
  lastName: string;
  id: number;
}
