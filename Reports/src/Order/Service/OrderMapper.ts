import { Injectable, Inject } from '@nestjs/common';
import { Repository } from './Repository';
import { getElementWithHighestOccurence } from 'src/helpers';
import { Customer } from '../Model/Customer';

@Injectable()
export class OrderMapper {
  @Inject() repository: Repository;

  async getOrdersForGivenDay(date: string) {
    const orders = await this.repository.fetchOrders();
    return orders.filter(order => order.createdAt === date);
  }

  async getBestsellers(date: string) {
    const dateOrders = await this.getOrdersForGivenDay(date);

    if (dateOrders.length < 1) {
      return null;
    }
    const productsIds = dateOrders.reduce((ids, order) => {
      return [...ids, ...order.products];
    }, []);

    const { id, count } = getElementWithHighestOccurence(productsIds);
    const { name, price } = await this.getProductByNumber(id);

    return {
      productName: name,
      quantity: count,
      totalPrice: count * price,
    };
  }

  async getProductByNumber(id: number) {
    const products = await this.repository.fetchProducts();
    return products.find(product => product.id === id);
  }

  async getBestBuyers(date: string) {
    const dateOrders = await this.getOrdersForGivenDay(date);

    if (dateOrders.length < 1) {
      return null;
    }

    const customersOrderValues = [];

    for (const order of dateOrders) {
      const { firstName, lastName, id } = await this.getCustomerById(
        order.customer,
      );
      const orderTotalPrice = await this.getOrderTotalPrice(order.products);

      if (!customersOrderValues[id]) {
        customersOrderValues[id] = {
          customerName: `${firstName} ${lastName}`,
          totalPrice: orderTotalPrice,
        };
      }
      customersOrderValues[id].totalPrice =
        customersOrderValues[id].totalPrice + orderTotalPrice;
    }

    return customersOrderValues
      .sort((a, b) => b.totalPrice - a.totalPrice)
      .shift();
  }

  async getOrderTotalPrice(productIds: number[]): Promise<number> {
    let totalPrice = 0;
    for (const productId of productIds) {
      const product = await this.getProductByNumber(productId);
      totalPrice += product.price;
    }

    return totalPrice;
  }

  async getCustomerById(id: number): Promise<Customer> {
    const customers = await this.repository.fetchCustomers();
    return customers.find(customer => customer.id === id);
  }
}
