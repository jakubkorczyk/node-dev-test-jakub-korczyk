export class BSort {
  /**
   * selectionSort: sorts given array of numbers with selection sort algorithm
   */
  public static selectionSort(array: number[]): number[] {
    const arrayCopy = array.slice(); 
    const length = arrayCopy.length;
    for (let i = 0; i < length; i++) {
        let min = i;
        for (let j = i + 1; j < length; j++) {
            if (arrayCopy[min] > arrayCopy[j]) {
                min = j;
            }
        }
        if (min !== i) {
            const tmp = arrayCopy[i];
            arrayCopy[i] = arrayCopy[min];
            arrayCopy[min] = tmp;
        }
    }
    return arrayCopy;
  }
}