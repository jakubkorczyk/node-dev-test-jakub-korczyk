import { ASort } from './ASort';
import { BSort } from './BSort';
import { BSearch } from './BSearch';

const unsorted = [13, 2, 17, 5, 77, 22, 83, 65, 14, 9, 0, 4, 7, 32];
const elementsToFind = [1, 5, 13, 27, 77];

const bubbleSorted = ASort.bubbleSort(unsorted);
const selectionSorted = BSort.selectionSort(unsorted);

console.log(`Bubble sort result: ${bubbleSorted}`);
console.log(`Selection sort result: ${selectionSorted}`);

const binarySearch = new BSearch();

elementsToFind.forEach(el => console.log(`Number found on position: ${binarySearch.search(bubbleSorted, el)}`));

console.log(`Count of binary search operations performed: ${binarySearch.getOperations()}`);