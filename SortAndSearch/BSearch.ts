export class BSearch {
  private operations: number = 0;

  public getOperations() {
    return this.operations;
  }
  /**
   * search: findes index of given element with binary search algorithm.
   */
  public search(array: number[], element: number, start: number = 0, end: number = array.length - 1) {
    this.operations++;
    if (start > end) {
      return -1;
    }
    const half = Math.floor((start + end) / 2);
    const current = array[half];
    if (current === element) {
      return half;
    } 
    if (current > element) {
      return this.search(array, element, start, half - 1);
    } else {
      return this.search(array, element, half + 1, end);
    }
  }
}