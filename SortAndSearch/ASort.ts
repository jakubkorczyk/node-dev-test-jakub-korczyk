export class ASort {
  /**
   * bubbleSort: sorts given array of numbers with bubble sort algorithm
   */
  public static bubbleSort(array: number[]): number[] {
    const arrayCopy = array.slice();
    for (let i = 0; i < arrayCopy.length; i++) {
      let outer = arrayCopy[i];
      for (let j = i + 1; j < arrayCopy.length; j++) {
        let inner = arrayCopy[j];
        if (outer > inner) {
          arrayCopy[i] = inner;
          arrayCopy[j] = outer;

          outer = arrayCopy[i];
          inner = arrayCopy[j];
        }
      }
    }
    return arrayCopy;
  }
}